<?php

namespace Drupal\gitea;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use GuzzleHttp\ClientInterface;

/**
 * Class to retrieve information about the configured repository.
 */
class Gitea {

  /**
   * The variable to get the repository.
   *
   * @var string
   */
  protected $repo;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * An http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
    MessengerInterface $messenger
  ) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->messenger = $messenger;
    $this->repo = $this->configFactory->get('gitea.settings')->get('repo');
  }

  /**
   * Get a repository.
   */
  public function getRepo() {
    return $this->sendRequest('/repos/' . $this->repo, []);
  }

  /**
   * Get default branch.
   */
  public function getDefaultBranch() {
    $repo = $this->getRepo();
    return $repo->default_branch;
  }

  /**
   * Get all branches for a repository.
   */
  public function getBranches() {
    return $this->sendRequest('/repos/' . $this->repo . '/branches', []);
  }

  /**
   * Get all branches for a repository as a options.
   */
  public function getBranchesAsOptions() {
    $options = [];
    if ($branches = $this->getBranches()) {
      foreach ($branches as $branch) {
        $key = $branch->name;
        $options[$key] = $key;
      }
    }
    return $options;
  }

  /**
   * Create a new branch.
   *
   * @param string $new_branch
   *   The name of New branch.
   * @param string $old_branch
   *   The name of Old branch.
   *
   * @return mixed
   *   Return branch.
   */
  public function createBranch(string $new_branch, string $old_branch = '') {
    $data['json']['new_branch_name'] = $new_branch;
    if ($old_branch) {
      $data['json']['old_branch_name'] = $old_branch;
    }
    return $this->sendRequest('/repos/' . $this->repo . '/branches', $data, 'post');
  }

  /**
   * Delete a branch.
   *
   * @param string $branch
   *   The branch name.
   *
   * @return mixed
   *   Return delete branch request.
   */
  public function deleteBranch(string $branch) {
    return $this->sendRequest('/repos/' . $this->repo . '/branches/' . $branch, [], 'delete');
  }

  /**
   * Create a new file.
   *
   * @param string $file_path
   *   The File path.
   * @param string $file_content
   *   The file content.
   * @param string $merge_from
   *   The merge from.
   * @param string $message
   *   The message.
   *
   * @return mixed
   *   File created request.
   */
  public function createFile(string $file_path, string $file_content, string $merge_from, string $message) {
    $data['json'] = [
      'branch' => $merge_from,
      'content' => base64_encode($file_content),
      'message' => $message,
    ];
    return $this->sendRequest('/repos/' . $this->repo . '/contents/' . $file_path, $data, 'post');
  }

  /**
   * Get file function.
   */
  public function getFile(string $file_path, string $branch) {
    $data['query'] = [
      'ref' => $branch,
    ];
    return $this->sendRequest('/repos/' . $this->repo . '/contents/' . $file_path, $data, 'get');
  }

  /**
   * Update a file.
   *
   * @param string $file_path
   *   The File path.
   * @param string $file_content
   *   The File content.
   * @param string $sha
   *   The sha.
   * @param string $merge_from
   *   The Merge from.
   * @param string $message
   *   The message.
   *
   * @return mixed
   *   Return update file request.
   */
  public function updateFile(string $file_path, string $file_content, string $sha, string $merge_from, string $message) {
    $data['json'] = [
      'branch' => $merge_from,
      'content' => base64_encode($file_content),
      'message' => $message,
      'sha' => $sha,
    ];
    return $this->sendRequest('/repos/' . $this->repo . '/contents/' . $file_path, $data, 'put');
  }

  /**
   * Update a file.
   *
   * @param string $file_path
   *   The File path.
   * @param string $sha
   *   The sha.
   * @param string $merge_from
   *   The Merge from.
   * @param string $message
   *   The message.
   *
   * @return mixed
   *   Return deletefile request.
   */
  public function deleteFile(string $file_path, string $sha, string $merge_from, string $message) {
    $data['json'] = [
      'branch' => $merge_from,
      'message' => $message,
      'sha' => $sha,
    ];
    return $this->sendRequest('/repos/' . $this->repo . '/contents/' . $file_path, $data, 'delete');
  }

  /**
   * Create a Pull Request.
   *
   * @param string $base
   *   The base.
   * @param string $head
   *   The head.
   * @param string $commit_message
   *   The commit message.
   *
   * @return mixed
   *   Return create pull request.
   */
  public function createPullRequest(string $base, string $head, string $commit_message) {
    $data['json'] = [
      'base' => $base,
      'head' => $head,
      'body' => $commit_message,
      'title' => 'Config patch',
    ];
    return $this->sendRequest('/repos/' . $this->repo . '/pulls', $data, 'post');
  }

  /**
   * Send the request.
   *
   * @param string $endpoint
   *   The endpoint.
   * @param array $data
   *   The data.
   * @param string $method
   *   The method.
   *
   * @return mixed
   *   Return boolean.
   */
  public function sendRequest($endpoint, array $data = [], $method = 'get') {
    $config = $this->configFactory->get('gitea.settings');
    $url = $config->get('repo_url') . '/api/v1' . $endpoint;

    $data['query']['access_token'] = $config->get('api_key');

    try {
      $request = $this->httpClient->request($method, $url, $data);
      return json_decode($request->getBody());
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
    }
    return FALSE;

  }

}
